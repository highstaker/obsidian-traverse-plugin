import { App, Editor, MarkdownView, Modal, Notice, Plugin, PluginSettingTab, Setting } from 'obsidian';


export default class MyPlugin extends Plugin {

	TRESHOLD_DAYS = 7

	determine_traverse_destination(saved_data: any, curfilename: string): string {
		// var randomProperty = function (obj: any) {
		// 	var keys = Object.keys(obj);
		// 	return keys[keys.length * Math.random() << 0];
		// };

		var curfile_data = saved_data[curfilename]
		if (!curfile_data) {
			return ""
		}
		var curfile_links = curfile_data["links"]

		var visited_files = new Set()
		visited_files.add(curfilename)

		const cur_time = new Date()

		var oldest_neighbour = ""
		var oldest_visit_time = cur_time
		var any_beyond_treshold = false
		for (var f in curfile_links) {
			if (!visited_files.has(f)) {
				visited_files.add(f)

				let last_visited: Date = new Date(saved_data[f]["last_visited"])

				let diff = Math.abs(cur_time.getTime() - last_visited.getTime());
				let diffDays = Math.ceil(diff / (1000 * 3600 * 24));

				if (last_visited.getTime() < oldest_visit_time.getTime()) {
					oldest_visit_time = last_visited
					oldest_neighbour = f
					if (diffDays >= this.TRESHOLD_DAYS) {
						any_beyond_treshold = true
					}
				}
			}
		}
		if (any_beyond_treshold) {
			return oldest_neighbour
		}

		var findOldestBfs = function (startNode: any) {
			const previous = new Map();
			const visited = new Set();
			const queue = [];
			queue.push(startNode);
			visited.add(startNode);

			let oldest_neighbour = startNode
			let oldest_visit_time: Date = new Date()

			while (queue.length > 0) {
				const node = queue.shift(); //Removes the first element from an array and returns it.
				let neighbours: any = saved_data[node]["links"]

				for (let neighbour in neighbours) {
					if (!visited.has(neighbour)) {
						previous.set(neighbour, node);
						queue.push(neighbour);
						visited.add(neighbour);

						let neighbour_last_visited = new Date(saved_data[neighbour]["last_visited"])
						if (neighbour_last_visited.getTime() < oldest_visit_time.getTime()) {
							oldest_visit_time = neighbour_last_visited
							oldest_neighbour = neighbour
						}
					}
				}
			}
			return oldest_neighbour;
		}

		var shortestPathBfs = function (startNode: any, stopNode: any) {
			const previous = new Map<string, string>();
			const visited = new Set();
			const queue = [];
			queue.push(startNode);
			visited.add(startNode);

			while (queue.length > 0) {
				const node = queue.shift(); //Removes the first element from an array and returns it.
				if (node === stopNode) {
					console.log("previous", previous)
					return previous.get(stopNode)
				}

				for (let neighbour in saved_data[node]["links"]) {
					if (!visited.has(neighbour)) {
						previous.set(neighbour, node);
						queue.push(neighbour);
						visited.add(neighbour);
					}
				}
			}

			return "";
		}

		var oldest = findOldestBfs(curfilename)
		console.info("Traversing towards: " + oldest)
		new Notice("Traversing towards: " + oldest)

		let target_neighbour = shortestPathBfs(oldest, curfilename)
		console.log("target_neighbour", target_neighbour)

		return target_neighbour || oldest_neighbour
	}

	async traverse_towards_oldest_visit() {
		let saved_data = await this.loadData()
		if (!saved_data) {
			saved_data = {}
		}

		const curfile = this.app.workspace.getActiveFile()
		if (!curfile) {
			return
		}
		const curfilename = curfile["name"]

		const chosen_file = this.determine_traverse_destination(saved_data, curfilename)
		if (chosen_file == "") {
			console.warn("Could not determine destination")
			return
		}

		const cur_time = new Date()

		saved_data[curfilename]["last_visited"] = cur_time
		saved_data[chosen_file]["last_visited"] = cur_time
		await this.saveData(saved_data)

		// open the file
		this.app.workspace.openLinkText(chosen_file, "", false, { "active": true })
	}

	async jump_to_oldest_visit() {
		let saved_data = await this.loadData()
		if (!saved_data) {
			saved_data = {}
		}

		const curfile = this.app.workspace.getActiveFile()
		if (!curfile) {
			return
		}
		const curfilename = curfile["path"]

		const cur_time = new Date()

		let oldest_visit_time = cur_time
		let oldest_neighbour = ""
		for (var d in saved_data) {
			let cur_visit_time = new Date(saved_data[d]["last_visited"])
			if (cur_visit_time.getTime() < oldest_visit_time.getTime()) {
				oldest_visit_time = cur_visit_time
				oldest_neighbour = d
			}
		}
		const chosen_file = oldest_neighbour
		if (chosen_file == "") {
			console.warn("Could not determine file to jump to")
			return
		}

		saved_data[curfilename]["last_visited"] = cur_time
		saved_data[chosen_file]["last_visited"] = cur_time
		await this.saveData(saved_data)

		this.app.workspace.openLinkText(chosen_file, "", false, { "active": true })
	}

	async update_data() {
		const file_list = this.app.vault.getMarkdownFiles()
		//on obsidian startup file_list is empty for some reason.
		//Let's just skip the update, or it'll erase the data
		if (file_list.length <= 0) {
			return
		}

		let existent_files = new Map() // will be needed for data cleanup later

		let saved_data = await this.loadData()
		if (!saved_data) {
			saved_data = {}
		}

		// creating an entry for each file in data
		for (var f of file_list) {
			let path = f["path"]
			existent_files.set(path, true)

			if (!saved_data[path]) {
				saved_data[path] = { "last_visited": new Date("1970-01-01"), "links": {} }
			}
			saved_data[path]["links"] = {}
		}

		// writing links and backlinks. To a set, to avoid duplicates.
		for (var f of file_list) {
			let path = f["path"] // path to current file

			const file_cache = this.app.metadataCache.getFileCache(f)
			if (file_cache) {
				let links = file_cache["links"] // links from current file
				if (links) {
					for (var l of links) {
						if (!saved_data[path]["links"]) {
							saved_data[path]["links"] = {}
						}

						let link_path = l["link"] + ".md" // path to link from current file

						// adding the linked file only if it exists. 
						//There are empty links that point to nothing, they should be omitted.
						if (existent_files.has(link_path)) {
							// writing a link to current file
							saved_data[path]["links"][link_path] = true

							// writing a backlink to the other file
							saved_data[link_path]["links"][path] = true
						}
					}
				}
			}
		}

		// removing deleted files
		for (var k in saved_data) {
			if (!existent_files.has(k)) {
				// a convoluted way of deleting a key
				const { [k]: _, ...b } = saved_data
				saved_data = b
			}
		}

		await this.saveData(saved_data)

		console.info("Traverse data updated")
		new Notice("Traverse data updated")
	}

	async onload() {

		// Possible icons can be looked up at https://lucide.dev/

		// This creates an icon in the left ribbon.
		const traverseIconEl = this.addRibbonIcon('redo', 'Traverse towards oldest', (evt: MouseEvent) => {
			// Called when the user clicks the icon.
			this.traverse_towards_oldest_visit();
		});
		// This creates an icon in the left ribbon.
		const jumpIconEl = this.addRibbonIcon('rocking-chair', 'Jump to oldest', (evt: MouseEvent) => {
			// Called when the user clicks the icon.
			this.jump_to_oldest_visit();
		});

		this.addCommand({
			id: 'traverse-towards-oldest-visit',
			name: 'Traverse towards oldest visit',
			callback: () => {
				this.traverse_towards_oldest_visit();
			}
		});

		this.addCommand({
			id: 'jump-to-oldest-visit',
			name: 'Jump to oldest visit',
			callback: () => {
				this.jump_to_oldest_visit();
			}
		});


		this.addCommand({
			id: 'update-traverse-data',
			name: 'Update traverse data',
			callback: () => {
				this.update_data();
			}
		});

		await this.update_data()
	}

	onunload() {

	}

}
